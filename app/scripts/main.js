////////Tracking Controller
var trackController = (function(){
    var Track = function(id, trackID, phoneNumber){
        this.id = id;
        this.trackID = trackID;
        this.phoneNumber = phoneNumber;
    }

    var data = {
        allTrack : []
    };

    return{
        addTracks : function(trckID, trcNum){
            var ID, newTrack;

            ///Create New ID
            if(data.allTrack.length > 0){
                ID = data.allTrack[data.allTrack.length -1].id +1;
            }
            else{
                ID =0;
            }

            ////Create New Item
            newTrack = new Track(ID, trckID, trcNum);

            /////Push it into datastructure
            data.allTrack.push(newTrack);

            /////Return newTrack
            return newTrack;
        },

        testing : function(){
              console.log(data);
        }
    };
})();




//////UI controller
var UIcontroller = (function(){
    var DOMstring = {
        trackID : ".trackid",
        phoneNumber : ".phoneNum",
        submitBtn : ".delivery-submit-button",
        trackContainer : ".track_list"
    };

    return{
        getDOMstring : function(){
            return DOMstring;
        },

        getInput : function(){
            return{
                trackID : document.querySelector(DOMstring.trackID).value,
                phoneNumber : document.querySelector(DOMstring.phoneNumber).value
            }
        },

        addTrackTerm : function(obj){
            var html, element, newHtml;
            element = DOMstring.trackContainer;
            ////create html with placeholdertext
            html = '<div class="item clearfix" id="income-%id%"> <div class="item__description">%trackID%</div> <div class="right clearfix"> <div class="item__value">%trackNumber%</div> <div class="item__delete"> <button class="item__delete--btn"><i class="fal fa-times-circle"></i></button></div></div></div>'
            
            ////Replace placeholder text with actual data
            newHtml = html.replace("%id%",obj.id);
            newHtml = newHtml.replace("%trackID%", obj.trackID);
            newHtml = newHtml.replace("%trackNumber%", obj.phoneNumber);

            ///Insert Html into DOm
            document.querySelector(element).insertAdjacentHTML("beforeend", newHtml);
        }
    };

})();




//////Global App controller
var controller = (function(UIctrl, trackctrl){
    var DOM = UIctrl.getDOMstring();
    var setupEventListener = function(){
        document.querySelector(DOM.submitBtn).addEventListener("click", ctrlItems);
    
        document.addEventListener("keypress", function(event){
            if(event.keyCode===13 || event.which===13){
                ctrlItems();
            }
        });
    };

    var ctrlItems = function(){
        var input, newItems;
        /////Get Field input
        input = UIctrl.getInput();
        console.log(input);

        ////Add them to trackController
        newItems = trackctrl.addTracks(input.trackID, input.phoneNumber);

        ////Add the item to the UI
        UIctrl.addTrackTerm(newItems);
    };

    return{
        init : function(){
            console.log("Initialization Started");
            setupEventListener();
        }
    };

    
})(UIcontroller,trackController);

controller.init();